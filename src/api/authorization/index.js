/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import { ref } from 'vue';

import {
  API_AUTHORIZATION_DOSAVE,
  API_AUTHORIZATION_GETFUNCFIELDDIC,
  API_AUTHORIZATION_GETPERMEDATA,
  API_AUTHORIZATION_LOADFUNC,
  API_AUTHORIZATION_LOADGRIDTREEDATA,
  API_RBAC_LOADDEPARTMENTTREE,
  API_RBAC_LOADORG,
  API_RBAC_LOADROLETREE,
  API_CUSTOMER_AUTHORIZATION_DOSAVE,
  API_CUSTOMER_AUTHORIZATION_GETPERMEDATA,
} from './urls';

/**
 * 自定义api配置
 */
export const customerApi = ref(false);
/**
 * 获取表编码
 * @returns
 */
function getTableCode() {
  return customerApi.value ? 'JE_CORE_CUSTOMER_FUNCPERM' : 'JE_CORE_FUNCPERM';
}

/**
 * 得到表单树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadGridTreeDataApi(params) {
  return ajax({ url: API_AUTHORIZATION_LOADGRIDTREEDATA, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getPremDataApi(params) {
  const url = customerApi.value
    ? API_CUSTOMER_AUTHORIZATION_GETPERMEDATA
    : API_AUTHORIZATION_GETPERMEDATA;
  params.tableCode = getTableCode();
  return ajax({ url, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 权限保存
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doSaveApi(params) {
  const url = customerApi.value ? API_CUSTOMER_AUTHORIZATION_DOSAVE : API_AUTHORIZATION_DOSAVE;
  params.tableCode = getTableCode();
  return ajax({ url, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 加载角色树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadRoleTreeApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADROLETREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取部门树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadDepartmentTreeApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADDEPARTMENTTREE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 查询全部机构
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadOrgApi(params) {
  return ajax({ method: 'GET', url: API_RBAC_LOADORG, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取功能用到的字典
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getFuncFieldDicApi(params) {
  return ajax({ url: API_AUTHORIZATION_GETFUNCFIELDDIC, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取功能用到的字典
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadFuncApi(params) {
  return ajax({ url: API_AUTHORIZATION_LOADFUNC, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
