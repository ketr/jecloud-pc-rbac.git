/**
 * API_URL命名规则：API_模块_方法
 */
// 加载数据
export const API_RBAC_MATRIX_LOAD = '/je/common/load';
// 添加数据
export const API_RBAC_MATRIX_DO_SAVE = '/je/common/doSave';
// 保存数据
export const API_RBAC_MATRIX_DO_UPDATE = '/je/common/doUpdate';
// 批量保存数据
export const API_RBAC_MATRIX_DO_UPDATE_LIST = '/je/common/doUpdateList';
// 删除数据
export const API_RBAC_MATRIX_DO_REMOVE = '/je/common/doRemove';
// 复制矩阵字段数据
export const API_RBAC_MATRIX_COPY_FILED = '/je/rbac/matrix/doCopy';
