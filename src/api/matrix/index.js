/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  API_RBAC_MATRIX_LOAD,
  API_RBAC_MATRIX_DO_SAVE,
  API_RBAC_MATRIX_DO_UPDATE,
  API_RBAC_MATRIX_DO_UPDATE_LIST,
  API_RBAC_MATRIX_DO_REMOVE,
  API_RBAC_MATRIX_COPY_FILED,
} from './urls';

/**
 * 加载数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadData(params) {
  return ajax({ url: API_RBAC_MATRIX_LOAD, params, headers: { pd: 'rbac' } }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 添加数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doSave(params) {
  return ajax({ url: API_RBAC_MATRIX_DO_SAVE, params, headers: { pd: 'rbac' } }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 修改数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdate(params) {
  return ajax({ url: API_RBAC_MATRIX_DO_UPDATE, params, headers: { pd: 'rbac' } }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 批量保存数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdateList(params) {
  return ajax({ url: API_RBAC_MATRIX_DO_UPDATE_LIST, params, headers: { pd: 'rbac' } }).then(
    (info) => {
      if (info.success) {
        return info.data;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 * 删除数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doRemove(params) {
  return ajax({ url: API_RBAC_MATRIX_DO_REMOVE, params, headers: { pd: 'rbac' } }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 复制矩阵字段数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doCopyFiled(params) {
  return ajax({ url: API_RBAC_MATRIX_COPY_FILED, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
